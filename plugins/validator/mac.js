const isValid = (mac) => {
	const reg = /^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$/;
	return reg.test(mac);
};

module.exports = {
	isValid,
};