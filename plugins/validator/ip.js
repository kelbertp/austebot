const isValid = (ip) => {
	const reg = /^((2[0-4]\d|25[0-5]|[01]?\d\d?)($|(?!\.$)\.)){4}$/;
	return reg.test(ip);
};

module.exports = {
	isValid,
};