const isValid = (port) => {
	const reg = /^([0-9]{1,5})$/;
	return reg.test(port) && port > 0 && port < 65536;
};

module.exports = {
	isValid,
};