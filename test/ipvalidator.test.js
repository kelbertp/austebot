const { Ip } = require ('../plugins');
const { expect, test } = require('@jest/globals');

test('IP Validation OK', () => {
	expect(Ip.isValid('12.157.35.17')).toBe(true);
});

test('IP Validation NOK - value sup 255', () => {
	expect(Ip.isValid('12.1575.35.17')).toBe(false);
	expect(Ip.isValid('12.155.35.')).toBe(false);
	expect(Ip.isValid('12.155.35')).toBe(false);
	expect(Ip.isValid('12.155.35.22.25')).toBe(false);
});