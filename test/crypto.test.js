const { Crypto } = require ('../plugins');
const { expect, test } = require('@jest/globals');

test('Test encrypt OK', () => {
	const text = 'test';
	const encrypted = Crypto.encrypt(text);
	expect(encrypted).not.toBe(text);
	const decrypted = Crypto.decrypt(encrypted);
	expect(decrypted).toBe(text);
});