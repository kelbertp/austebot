const { Mac } = require ('../plugins');
const { expect, test } = require('@jest/globals');

test('mac valid Ok', () => {
	expect(Mac.isValid('00:00:00:00:00:00')).toBe(true);
});

test('mac valid NOk', () => {
	expect(Mac.isValid('00:00:00:00:00:0')).toBe(false);
	expect(Mac.isValid('00:0G:00:00:00:00')).toBe(false);
	expect(Mac.isValid('0:0G:00:00:00:00')).toBe(false);
	expect(Mac.isValid('0:00:00:00:00:00:00')).toBe(false);
	expect(Mac.isValid(':00:00:00:00:00:00')).toBe(false);
});