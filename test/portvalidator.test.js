const { Port } = require ('../plugins');
const { expect, test } = require('@jest/globals');

test('port valid Ok', () => {
	expect(Port.isValid('65535')).toBe(true);
	expect(Port.isValid('25489')).toBe(true);
});

test('port valid NOk', () => {
	expect(Port.isValid('0')).toBe(false);
	expect(Port.isValid('-25489')).toBe(false);
	expect(Port.isValid('65536')).toBe(false);
});