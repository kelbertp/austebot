const mongoose = require('mongoose');

const pcSchema = mongoose.Schema(
	{
		name: {
			type: String,
			required: true,
			unique: true,
		},
		ipAdress: {
			type: String,
			required: true,
			trim: true,
			lowercase: true,
		},
		mac: {
			type: String,
			required: true,
			trim: true,
			lowercase: true,
		},
		port: {
			type: String,
			required: true,
			trim: true,
		},
	},
);

const Pc = mongoose.model('pc', pcSchema);

module.exports = Pc;