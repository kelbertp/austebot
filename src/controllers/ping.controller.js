const { EmbedBuilder } = require('discord.js');
const { alveariumApp, alveariumApi } = require('../../config.json');
const ping = require('ping');

const defaultPing = async (interaction) => {
	const embed = new EmbedBuilder()
		.setTitle('Commande Ping')
		.setDescription('Pong !')
		.setColor(0x00FFFF);
	await interaction.reply({ embeds: [embed], ephemeral:true });
	return;
};

const austeAppPing = async (interaction) => {
	const res = await ping.promise.probe(alveariumApp);
	if (res.alive) {
		const embed = new EmbedBuilder()
			.setTitle('Ping du site d\'Austénite')
			.setDescription(`Le site d'Austénite (APP) est en ligne et a été pingé en ${res.time}ms.`)
			.setColor(0x00FF00);
		interaction.reply({ embeds: [embed] });
	}
	else {
		const embed = new EmbedBuilder()
			.setTitle('Ping du site d\'Austénite')
			.setDescription('Le site d\'Austénite (APP) est hors ligne.')
			.setColor(0xFF0000);
		interaction.reply({ embeds: [embed] });
	}
	return;
};

const austeApiPing = async (interaction) => {
	const res = await ping.promise.probe(alveariumApi);
	if (res.alive) {
		const embed = new EmbedBuilder()
			.setTitle('Ping du site d\'Austénite')
			.setDescription(`Le site d'Austénite (API) est en ligne et a été pingé en ${res.time}ms.`)
			.setColor(0x00FF00);
		interaction.reply({ embeds: [embed] });
	}
	else {
		const embed = new EmbedBuilder()
			.setTitle('Ping du site d\'Austénite')
			.setDescription('Le site d\'Austénite (API) est hors ligne.')
			.setColor(0xFF0000);
		interaction.reply({ embeds: [embed] });
	}
	return;
};

module.exports = {
	defaultPing,
	austeAppPing,
	austeApiPing,
};