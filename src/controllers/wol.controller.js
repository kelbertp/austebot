const { EmbedBuilder } = require('discord.js');
const { WolService } = require('../services');
const Wol = require('node-wol');
const { Crypto, Ip, Mac, Port } = require ('../../plugins');

const add = async (interaction) => {
	const name = interaction.options.getString('name');
	const ipAdress = interaction.options.getString('ip');
	const mac = interaction.options.getString('mac');
	const port = interaction.options.getString('port');
	if (Ip.isValid(ipAdress) && Mac.isValid(mac) && Port.isValid(port)) {
		// const res = await WolService.createPc(
		// 	{
		// 		name,
		// 		ipAdress: Crypto.encrypt(ipAdress),
		// 		mac: Crypto.encrypt(mac),
		// 		port: Crypto.encrypt(port),
		// 	},
		// );
		const res = await WolService.createPc(
			{
				name,
				ipAdress,
				mac,
				port,
			},
		);
		if (res) {
			const embed = new EmbedBuilder()
				.setTitle('Ajout d\'un PC')
				.setDescription('Le PC a bien été ajouté')
				.setColor(0x00ff00);
			await interaction.reply({ embeds: [embed], ephemeral:true });
			return;
		}
	}
	const embed = new EmbedBuilder()
		.setTitle('Ajout d\'un PC')
		.setDescription('Le PC n\'a pas pu être ajouté.')
		.setColor(0xff0000);
	await interaction.reply({ embeds: [embed], ephemeral:true });
	return;
};

const help = async (interaction) => {
	const embed = new EmbedBuilder()
		.setTitle('Commande WOL')
		.setDescription(`
			**/wol help** : Affiche l'aide
			**/wol add <ip>** : Ajoute un PC à démarrer
			**/wol on** : Démarrer un PC
		`)
		.setColor(0x00FFFF);
	await interaction.reply({ embeds: [embed], ephemeral:true });
	return;
};

const on = async (interaction) => {
	const name = interaction.options.getString('nom');
	const pc = await WolService.getPcByName(name);
	if (pc) {
		const embed = new EmbedBuilder()
			.setTitle('Démarrage d\'un PC')
			.setDescription(`Démarrage de ${name}...`)
			.setColor(0x00ff00);
		// const mac = Crypto.decrypt(pc.mac);
		// console.log(mac);
		Wol.wake(pc.mac, { address: pc.ipAdress, port: pc.port }, (err) => {
			if (err) {
				embed.setDescription(`Le PC ${name} n'a pas pu être démarré.`);
				embed.setColor(0xff0000);
			}
			interaction.reply({ embeds: [embed], ephemeral:true });
		});
		// Wol.wake(Crypto.decrypt(pc.mac), { address: Crypto.decrypt(pc.ipAdress), port: Crypto.decrypt(pc.port) });
		setTimeout(async () => {
			embed.setDescription(`Le PC ${name} a bien été démarré !`);
			await interaction.editReply({ embeds: [embed] });
		}, 1000);
		return;
	}
	const embed = new EmbedBuilder()
		.setTitle('Démarrage d\'un PC')
		.setDescription(`Le PC ${name} n'existe pas`)
		.setColor(0xff0000);
	await interaction.reply({ embeds: [embed], ephemeral:true });
	return;
};

module.exports = {
	add,
	help,
	on,
};