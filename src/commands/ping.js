const { SlashCommandBuilder } = require('discord.js');
const PingController = require('../controllers/ping.controller');

const data = new SlashCommandBuilder()
	.setName('ping')
	.setDescription('Replies with Pong!')
	.addSubcommand(austePing =>
		austePing
			.setName('default')
			.setDescription('Répond avec Pong!'))
	.addSubcommand(austeAppPing =>
		austeAppPing
			.setName('alvearium-app')
			.setDescription('Ping le site d\'Austénite'))
	.addSubcommand(austeApiPing =>
		austeApiPing
			.setName('alvearium-api')
			.setDescription('Ping le site d\'Austénite'));

const execute = async (interaction) => {
	switch (interaction.options.getSubcommand()) {
	case 'default':
		PingController.defaultPing(interaction);
		return;
	case 'alvearium-app':
		PingController.austeAppPing(interaction);
		return;
	case 'alvearium-api':
		PingController.austeApiPing(interaction);
		return;
	}
};

module.exports = {
	data,
	execute,
};