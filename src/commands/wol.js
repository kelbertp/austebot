const { SlashCommandBuilder } = require('discord.js');
const WolController = require('../controllers/wol.controller');

const data = new SlashCommandBuilder()
	.setName('wol')
	.setDescription('Allumer un pc')
	.addSubcommand(helpCommand =>
		helpCommand
			.setName('help')
			.setDescription('Affiche l\'aide'),
	)
	.addSubcommand(addCommand =>
		addCommand
			.setName('add')
			.setDescription('Ajouter un PC à démarrer')
			.addStringOption(option => option.setName('name').setDescription('Nom du PC').setRequired(true))
			.addStringOption(option => option.setName('ip').setDescription('IP de l\'hôte').setRequired(true))
			.addStringOption(option => option.setName('port').setDescription('Port de l\'hôte').setRequired(true))
			.addStringOption(option => option.setName('mac').setDescription('Adresse MAC du pc à démarrer').setRequired(true)),
	)
	.addSubcommand(onCommand =>
		onCommand
			.setName('on')
			.setDescription('Démarrer un PC')
			.addStringOption(option => option.setName('nom').setDescription('Nom du PC').setRequired(true)));

const execute = async (interaction) => {
	switch (interaction.options.getSubcommand()) {
	case 'help':
		WolController.help(interaction);
		return;
	case 'add':
		WolController.add(interaction);
		return;
	case 'on':
		WolController.on(interaction);
		return;
	}
};

module.exports = {
	data,
	execute,
};