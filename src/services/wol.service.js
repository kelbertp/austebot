const { Pc } = require('../models');

const createPc = async (data) => {
	return Pc.create(data);
};

const getPcByName = async (name) => {
	return Pc.findOne({ name });
};

module.exports = {
	createPc,
	getPcByName,
};